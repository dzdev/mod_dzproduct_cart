<?php
/**
 * @version     1.0.0
 * @package     mod_dzproduct_cart
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <dev@dezign.vn> - dezign.vn
 */
 
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// Include the syndicate functions only once
require_once __DIR__ . '/helper.php';
require_once JPATH_SITE.'/components/com_dzproduct/helpers/route.php';
JFactory::getLanguage()->load('com_dzproduct', JPATH_SITE);

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));

// Display template
require JModuleHelper::getLayoutPath('mod_dzproduct_cart', $params->get('layout', 'default'));
