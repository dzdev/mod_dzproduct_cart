jQuery(document).ready(function(){
    // Update total price
    var updatePrice = function() {
        var cart = jQuery.cookie('cart');
        var total, id;
        total = 0;
        if (typeof cart !== 'undefined') {
            for (var id in cart) {
                total += cart[id].price * cart[id].quantity;
            }
        }
        jQuery('#module-cart-total-price').text(total);
    }
    jQuery(document).on('cart-updated', updatePrice);
    
    var renderCart = function(animation) {
        var cart = jQuery.cookie('cart');
        var row, id;
        if (typeof animation === 'undefined')
            animation = false;
        if (typeof cart !== 'undefined') {
            // Remove old cart row
            jQuery('table#module-cart-table > tbody > *').remove();
            
            for (id in cart) {
                row = '<tr>';
                
                row += '<td class="item-title">';
                row += cart[id].title;
                row += '</td>';
                
                row += '<td class="item-image">';
                row += '<img src="' + Joomla.rootUrl + cart[id].image + '" />';
                row += '</td>';
                
                row += '<td class="item-price">';
                row += '<span>' + cart[id].price + '</span>';
                row += '</td>';
                
                row += '<td class="item-quantity">';
                row += '<span>' + cart[id].quantity + '</span>';
                row += '<button type="button" class="btn btn-link btn-remove">Remove</button>';
                row += '</td>';
                
                row += '</tr>';
                row = jQuery(row); // Make this a DOM object
                
                quantityControl(row, id);
                if (animation)
                    row.hide().appendTo('table#module-cart-table > tbody').fadeIn(1000);
                else
                    row.appendTo('table#module-cart-table > tbody');
                jQuery('#module-cart-table').show();
            }
        }
    }
    
    // Attach handler to control the quantity for a row
    var quantityControl = function(row, id) {
        jQuery('.btn-remove', row).on('click', function() {
            var cart = jQuery.cookie('cart');
            delete cart[id];
            jQuery(row).fadeOut(300, function() {jQuery(row).remove()});
            jQuery.cookie('cart', cart);
//             jQuery(document).trigger('cart-updated');
        });
    }
    
    // Re-render on each cart update
    jQuery(document).on('cart-updated', function() {
        renderCart();
    });
    // Start up
    renderCart(true);
    updatePrice();
});
