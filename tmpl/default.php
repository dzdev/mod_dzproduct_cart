<?php
/**
 * @version     1.0.0
 * @package     mod_dzproduct_cart
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <dev@dezign.vn> - dezign.vn
 */
 
// no direct access
defined('_JEXEC') or die;
JHtml::_('script', 'com_dzproduct/jquery.cookie.js', true, true);
JHtml::_('script', 'mod_dzproduct_cart/cart.js', true, true);
JFactory::getDocument()->addScriptDeclaration('Joomla.rootUrl = "'.JUri::root().'";');
?>
<div class="dzproduct_cart-module<?php echo $moduleclass_sfx; ?>">
    <table id="module-cart-table" class="table table-striped" style="display:none;">
        <thead>
            <tr>
                <th><?php echo JText::_('COM_DZPRODUCT_ORDER_TITLE'); ?></th>
                <th width="20%"><?php echo JText::_('COM_DZPRODUCT_ORDER_IMAGE'); ?></th>
                <th><?php echo JText::_('COM_DZPRODUCT_ORDER_PRICE'); ?></th>
                <th class="text-center"><?php echo JText::_('COM_DZPRODUCT_ORDER_QUANTITY'); ?></th>
            </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="3" class="text-right"><strong><?php echo JText::_('COM_DZPRODUCT_ORDER_TOTAL_PRICE'); ?></strong></td>
                <td id="module-cart-total-price"></td>
        </tfoot>
    </table>
    <a href="<?php echo JRoute::_(DZProductHelperRoute::getOrderRoute()); ?>">
        <?php echo JText::_('MOD_DZPRODUCT_VIEW_CART'); ?>
    </a>
</div>